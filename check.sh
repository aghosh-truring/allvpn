#!/bin/bash

CHECK_DIR=/home/k4admin

if [ ! -d "${CHECK_DIR}/vesselJSON" ]
then
	echo "Wrong system!"
	exit 1
fi

while getopts 'iornN' OPTION
do
	case ${OPTION} in
		o)
			S_OK=yes
			;;
		i)
			S_IGNORE=yes
			;;
		r)
			S_FAIL=yes
			;;
		n)
			S_NOIGNORE=yes
			;;
		N)
			S_NOFILEIGNORE=yes
			;;
	esac
done

shift $(( OPTIND - 1 ))

echo '-- '`date +'%Y-%m-%d %H:%M:%S %z'`

SOFTETHER_SESSIONS=`/usr/local/vpnserver/vpncmd 127.0.0.1 /server /hub:vpn /cmd sessionlist`
for F in ${CHECK_DIR}/vesselJSON/[Mm][Yy][-_]*.json ${CHECK_DIR}/vesselJSON/[Cc][Vv][-_]*.json
do
	if [ -d ${CHECK_DIR}/.k4ignore ]
	then
		FILENAME=`basename ${F}`
		N_IGNORE=`cat ${CHECK_DIR}/.k4ignore/* | grep -i '^'${FILENAME}'$' | wc -l`
	else
		N_IGNORE=0
	fi

	if [ "${N_IGNORE}" != "0" ]
	then
		if [ ! -v S_NOFILEIGNORE ]
		then
			[ -v S_FILEIGNORE ] && echo '    File <'${FILENAME}'> ignored'
			continue
		fi
	fi

	S_IP=`jq -r '.vpn.ip_addr' < ${F}`
	S_LOGIN=`jq -r '.vpn.login' < ${F}`

	if [ -d ${CHECK_DIR}/.k4ignore ]
	then
		N_IGNORE=`cat ${CHECK_DIR}/.k4ignore/* | grep -i '^'${S_LOGIN}'$' | wc -l`
	else
		N_IGNORE=0
	fi

	if [ "${N_IGNORE}" != "0" ]
	then
		if [ ! -v S_NOIGNORE ]
		then
			[ -v S_IGNORE ] && echo '    '${S_LOGIN} ignored
			continue
		fi
	fi

	N_SESS=`awk '/^User Name/ {n=index($0,"|"); nm=substr($0,n+1); print(nm)}' <<< ${SOFTETHER_SESSIONS} | grep -i '^'${S_LOGIN}'$' | wc -l`
	if (( N_SESS == 0 ))
	then
		[ ! -v S_FAIL ] && echo '    '${S_LOGIN} disconnected
	elif  ! ping -n -q -c 1 -w 3 ${S_IP} > /dev/null 2>&1
	then
		if  ! ncat ${S_IP} 22 <<< '' > /dev/null 2>&1
		then
			if  ! ncat ${S_IP} 80 <<< 'GET /' > /dev/null 2>&1
			then
				[ ! -v S_FAIL ] && echo '    '${S_LOGIN} '('${S_IP}') unreachable'
			else
				[ -v S_OK ] && echo '    '${S_LOGIN} 'ok [missed ping & ssh probe]'
			fi
		else
			[ -v S_OK ] && echo '    '${S_LOGIN} 'ok [missed ping]'
		fi
	else
		[ -v S_OK ] && echo '    '${S_LOGIN} ok
	fi
done
