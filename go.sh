#!/bin/bash

if [ -d /var/ship_details ]
then
	GODIR="/var/ship_details"
	SUFFIX=""
	SERVERTYPE="openvpn"
elif [ -d "/home/k4admin/vesselJSON" ]
then
	GODIR="/home/k4admin/vesselJSON"
	SUFFIX=".json"
	SERVERTYPE="softether"
else
	echo "no softether, no openvpn"
	exit 5
fi

F1=${1}${SUFFIX}
F2=*${1}${SUFFIX}
F3=*${1}*${SUFFIX}

declare -a EXPANSION
EXPANSION[0]=`find ${GODIR} -name "$F1"`
EXPANSION[1]=`find ${GODIR} -iname "$F1"`
EXPANSION[2]=`find ${GODIR} -name "$F2"`
EXPANSION[3]=`find ${GODIR} -iname "$F2"`
EXPANSION[4]=`find ${GODIR} -name "$F3"`
EXPANSION[5]=`find ${GODIR} -iname "$F3"`

F=""

declare -a KKK
declare -A KKKK

for K in "${EXPANSION[@]}"
do
	if [ "${K}" == "" ]
	then
		continue
	fi

	KK=( ${K} )

	for N in "${KK[@]}"
	do
		if [ -v KKKK["$N"] ]
		then
			true
		else
			KKK+=( "$N" )
			KKKK["$N"]=$N
		fi
	done
done

echo '----'
for K in "${KKK[@]}"
do
	echo -n '   '
	basename $K
done
echo '----'

for K in "${EXPANSION[@]}"
do
	if [ "${K}" == "" ]
	then
		continue
	fi

	KK=(${K})

	if [ "${#KK[@]}" == 1 ]
	then
		F=${KK[0]}
		break
	else
		echo "ambiguous"
		exit 2
	fi
done

if [ "${F}" == "" ]
then
	echo not found
	exit 1
else
	if [ "${SERVERTYPE}" == "softether" ]
	then
		S_IP=`jq -r '.vpn.ip_addr' < ${F}`
		S_LOGIN=`jq -r '.vpn.login' < ${F}`
		N_SESS=`/usr/local/vpnserver/vpncmd 127.0.0.1 /server /hub:vpn /cmd sessionlist | awk '/^User Name/ {n=index($0,"|"); nm=substr($0,n+1); print(nm)}' | grep -i '^'${S_LOGIN}'$' | wc -l`
		if (( N_SESS > 0 ))
		then
			echo ${S_LOGIN} has ${N_SESS} 'softether session(s) active'
		fi
	elif [ "${SERVERTYPE}" == "openvpn" ]
	then
		S_IP=`cat ${F}`
		NOW=`date +'%s'`
		THEN=`date -r ${F} +'%s'`
		echo 'Last contact was' $((NOW - THEN)) 'seconds ago'
	else
		echo "how did we get here?"
		exit 6
	fi

	echo `basename $F` $S_IP
	echo '===='
fi

if [ "$#" == "2" ]
then
	LOGIN_AS=${2}
elif [[ "$1" =~ ^ite.*$ ]]
then
	LOGIN_AS="k4mobility"
else
	LOGIN_AS="k4mobility"
fi

#LOGIN_AS=${2:-k4admin}
echo 
echo 
ssh ${LOGIN_AS}@${S_IP}
